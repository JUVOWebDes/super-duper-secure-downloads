<?php

namespace SD2;

use diversen\sendfile;

class SecureDownload
{

    public function __construct() {
        add_action("init", [$this, "maybeSetSession"]);
    }

    public function maybeSetSession() {
        if (!session_id()) {
            session_start();
        }
    }

    /**
     * Creates session for secure downloads
     */
    private function saveInSession(array $setter = null): void {
        if (!empty($setter)) {
            foreach ($setter as $key => $set) {
                if (!$key || !$set) {
                    continue;
                }
                $_SESSION["secure-downloads-" . $key] = $set;
            }
        }
    }

    private function readFromSession(array $getter = null, bool $removeAfterGet = false): array {
        // Outputs complete Session per default
        $output = array();

        if (!empty($getter) && !empty($_SESSION)) {
            $output = array();
            foreach ($getter as $get) {
                if (empty($_SESSION["secure-downloads-" . $get])) {
                    continue;
                }

                $output[$get] = $_SESSION["secure-downloads-" . $get];

                if ($removeAfterGet) {
                    unset($_SESSION["secure-downloads-" . $get]);
                }
            }
        }

        return $output;
    }

    /**
     * Creates a zip folder for multiple files
     *
     * @param array $files of post ids
     * @return string to created zip file
     */
    private function maybeZip(array $files): string {

        // Single File
        if (count($files) == 1) {
            $file = get_attached_file($files[0]);

            if (!file_exists($file)) {
                return false;
            }

            return $file;
        }


        // Prepare files
        foreach ($files as $file) {

            $type = get_post_type($file);
            if ($type == "attachment") {
                $file = get_attached_file($file);
            }

            if (!file_exists($file)) {
                return false;
            }

            $filesForZip[] = $file;

        };

        //Create Zip
        $zipPath = get_temp_dir() . "download.zip";
        $zip = new ZipArchive();
        if ($zip->open($zipPath, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) !== TRUE) {
            die ("An error occurred creating your ZIP file.");
        }

        foreach ($filesForZip as $file) {
            // Push file to zip
            $zip->addFile($file, basename($file));
        }


        //Finish Zip
        $zip->close();

        return $zipPath;
    }

    /**
     * Super Duper Master Download Function
     *
     * Pushes Download to client, if the entity contains multiple files, a zip is created.
     *
     * @param $entityToDownload  file | Array with file paths
     * @param string|null $origin
     * @throws \Exception
     */
    public function secureDownload($entityToDownload, string $origin = null) {

        $deleteAfterDownload = false;

        // If no entity to download exit
        if (empty($entityToDownload)) {
            return;
        }

        // Set WordPress user
        !wp_get_current_user()->ID == 0 ? $user = wp_get_current_user() : $user = null;

        $allowed = apply_filters('secure_download_permission', false, $entityToDownload, $user, $origin);
        // Exit if user is not allowed
        if ($allowed != true) {
            $this->saveInSession(["error" => $this->codeToError(403), "lastEntity" => $entityToDownload]);
            return;
        }


        switch (true) {
            // Zip if multiple files to download
            case is_array($entityToDownload):
                $zip = $this->maybeZip($entityToDownload);
                if ($zip == false || !file_exists($zip)) {
                    $this->saveInSession(["error" => $this->codeToError(404), "lastEntity" => $entityToDownload]);
                    return;
                }
                $deleteAfterDownload = true;
                break;
            // WordPress Attachment
            case get_post_type($entityToDownload) == "attachment":
                $file = get_attached_file($entityToDownload);
                if (!file_exists($file)) {
                    $this->saveInSession(["error" => $this->codeToError(404), "lastEntity" => $entityToDownload]);
                    return;
                }
                $path = $file;
                break;
            // Path to file
            case is_string($entityToDownload):
                if (!file_exists($entityToDownload)) {
                    $this->saveInSession(["error" => $this->codeToError(404), "lastEntity" => $entityToDownload]);
                    return;
                }
                $path = $entityToDownload;
                break;
        }


        //Push Download to Browser
        try {
            $s = new sendfile();
            $s->send($path);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

        if ($deleteAfterDownload && file_exists($path)) {
            unlink($path);
        } else {
            throw new \Exception("File '$path' could not be deleted after download."); //How to send error code
        }

        // Remove possible errors from previous 
        if (isset($_SESSION["juvo-nasher-download"])) {
            unlink($_SESSION["juvo-nasher-download"]);
        }
    }

    private function ifMissingAddTrailingSlash(string $path) {

        if ($path && substr($path, -1) != "/") {
            return $path . "/";
        }

        return $path;
    }

    /**
     * Make Url with params for download with an array containing values
     */
    public function arrayToUrlParams(array $files, int $postID): string {

        if (!$files || !$postID) {
            return "";
        }

        $url = "?action=download&post=$postID";


        foreach ($files as $file) {
            $url .= "&files[]=" . $file["ID"];
        }

        return $url;
    }

    /**
     * Maps an error Code to its error message
     *
     * @return array with error code and message
     */
    public function codeToError(int $code): array {

        $error = [
            "code"    => $code,
            "message" => null
        ];

        switch ($code) {
            case 403:
                $error["message"] = __("You dont have the permission to download this file", "juvo_nasher");
                break;
            case 404:
                $error["message"] = __("The file you requested is not available", "juvo_nasher");
                break;
        }

        return $error;

    }

    public function getError(): array {
        $keysToGet = ["error", "lastEntity"];
        $result = $this->readFromSession($keysToGet, true);

        if (empty($result)) {
            return array();
        }

        if (is_array($result) && count($result) == 1) {
            foreach ($result as $singleResult) {
                return $singleResult;
            }
        }

        return $result;
    }

}