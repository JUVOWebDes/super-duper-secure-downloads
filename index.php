<?php
/**
 * Plugin Name: Super Duper Secure Downloads
 * Description: This plugin handles downloads without revealing the real source path
 * Version:      1.0
 * Author: JUVO Webdesign
 * Author URI: http://juvo-design.de/
 * Text Domain: sd2
 * Domain Path /languages
 */

// Prevent direct access
if (!defined('ABSPATH')) {
    exit;
}

// Define constants.
define( 'SD2_PATH', plugin_dir_path( __FILE__ ) );
define( 'SD2_URL', plugin_dir_url(__FILE__));
define( 'SD2_BASENAME', plugin_basename( __FILE__ ) );

require_once( SD2_PATH . "Autoload.php");
spl_autoload_register( [ new SD2\Autoload( 'SD2', realpath(plugin_dir_path(__FILE__).'/src') ) , 'load' ] );

//Plugin Initialisation
add_action( 'plugins_loaded', array( new SuperDuperSecureDownloads(), 'pluginSetup' ) );
class SuperDuperSecureDownloads {

    public function pluginSetup() {
        $this->load_language();
    }

    public function load_language() {
        load_plugin_textdomain(
            "sd2",
            FALSE,
            dirname(SD2_BASENAME) . '/languages'
        );
    }

}